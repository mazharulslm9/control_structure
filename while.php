<?php
/*while (expr)
    statement -----or----
while (expr):
    statement
    ...
endwhile; */ 
$i = 1;
while ($i <= 10) {
    echo $i++;  /* the printed value would be
                   $i before the increment
                   (post-increment) */
}
$i = 1;
while ($i <= 10):
    echo $i;
    $i++;
endwhile;